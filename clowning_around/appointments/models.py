from django.db.models.deletion import SET_NULL
from clowning_around.users.models import Troupe
from django.db import models
from users.models import Client, TroupeLeader, Clown

class Appointment(models.Model):
    date_created = models.DateTimeField()
    date_of_appointment = models.DateTimeField()
    client = models.ForeignKey(Client, on_delete=models.SET_NULL)
    troupe_leader = models.ForeignKey(TroupeLeader, on_delete=SET_NULL)
    status = models.CharField(max_length=10)


class AppointmentIssues(models.Model):
    appointment = models.ForeignKey(Appointment, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=200)

class ClownsViewClientDetails(models.Model):
    appointment = models.ForeignKey(Appointment, on_delete=models.CASCADE)
    clown = models.ForeignKey(Clown, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    reason = models.CharField(max_length=100)