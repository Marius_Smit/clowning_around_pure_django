from django.urls import path

urlpatterns = [
    # clients:
    path('clients/appointments', ClientsAppointmentsView.as_view()),
    path('clients/appointments/upcoming', ClientsAppointmentsView.as_view()),
    path('clients/appointments/past', ClientsAppointmentsView.as_view()),
    path('clients/appointments/<int:pk>/rate', ClientsAppointmentsRateView.as_view()),

    # troupe leaders:
    path('troupeleader/createappointment', TroupeLeaderCreateAppointmentView.as_view()),

    # clowns:
    path('clowns/appointments', ClownsAppointmentsView.as_view()),
    path('clowns/appointments/<int:pk>', ClownsAppointmentsUpdateView.as_view()),
    path('clowns/appointments/<int:pk>/issue', ClownsAppointmentsIssueCreateView.as_view()),
    path('clowns/appointments/<int:pk>/clientdetails', ClownsAppointmentsIssueCreateView.as_view()),
]